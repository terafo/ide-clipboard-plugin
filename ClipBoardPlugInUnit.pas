unit ClipBoardPlugInUnit;

interface

uses
  Windows, SysUtils, Classes, ToolsAPI, vcl.Menus;

type
  TClipboardWizard = class(TInterfacedObject, IOTAWizard)
  private
    FMainMenuItem, FCopyMenuItem, FPasteMenuItem:  TMenuItem;

    // Formatting
    function GetFormattedString: string;
    function RemoveUnneededChars(const Value: string): string;

    // Menu events
    procedure CopyToClipboard(Sender: TObject);
    procedure PasteFromClipboard(Sender: TObject);
  public
    // TObject
    constructor Create;
    destructor Destroy; override;

    // IOTANotifier
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;

    // IOTAWizard
    function GetIDString: string;
    function GetName: string;
    function GetState: TWizardState;
    procedure Execute;
  end;

procedure Register;

implementation

uses
  Vcl.Clipbrd, System.StrUtils;

procedure Register;
begin
  RegisterPackageWizard(TClipboardWizard.Create);
end;

// Formatting

function TClipboardWizard.RemoveUnneededChars(const Value: string): string;
var
  List: TStringList;
  q: integer;
  s : string;
begin
  if Trim(Value) <> '' then
  begin
    List := TStringList.Create;
    try
      List.Text := Value;
      for q := 0 to List.Count - 1 do
      begin
        s := Trim(List[q]);
        if Length(s) > 0 then
          if s[1] = '''' then
            s := Copy(s, 2, Length(s));

        s := TrimLeft(ReverseString(s));

        if Length(s) > 0 then
          if s[1] = '+' then
            s := TrimLeft(Copy(s, 2, Length(s)));

        if Length(s) > 0 then
          if s[1] = ';' then
            s := TrimLeft(Copy(s, 2, Length(s)));

        if Length(s) > 0 then
          if s[1] = '''' then
            s := TrimLeft(Copy(s, 2, Length(s)));

        s := StringReplace(s, '''''', '''', [rfReplaceAll]);

        List[q] := ReverseString(s)
      end;

      Result := List.Text;
    finally
      List.Free;
    end;
  end
  else
    Result := '';
end;

procedure TClipboardWizard.CopyToClipboard(Sender: TObject);
begin
  with BorlandIDEServices as IOTAEditorServices do
    if Assigned(TopView) then
      Clipboard.AsText := RemoveUnneededChars(TopView.Block.Text);
end;

function TClipboardWizard.GetFormattedString: string;
const
  FSingleQuote = '''';
  Indent: array [boolean] of string = ('  ', '');
  EndChar: array [boolean] of string = (' +', ';');
var
  List: TStringlist;
  q: Integer;
begin
  if Clipboard.HasFormat(CF_TEXT) then
  begin
    List := TStringlist.Create;
    try
      List.Text := Clipboard.AsText;

      for q := 0 to List.Count - 1 do
        List[q] := Indent[q <> 0] + AnsiQuotedStr(TrimRight(List[q]) + #32, FSingleQuote) +
                   EndChar[q = (List.Count - 1)];

      Result := List.Text;
    finally
      List.Free;
    end;
  end;
end;

procedure TClipboardWizard.PasteFromClipboard(Sender: TObject);
begin
  with BorlandIDEServices as IOTAEditorServices do
    if Assigned(TopView) then
    begin
       TopView.Buffer.EditPosition.InsertText(GetFormattedString);
       TopView.Paint; // invalidation
    end;
end;


{ Anything else }
constructor TClipboardWizard.Create;
var
  NTAServices : INTAServices;
begin
  NTAServices := BorlandIDEServices as INTAServices;

  // Main Menu
  FMainMenuItem := TMenuItem.Create(nil);
  FMainMenuItem.Caption := 'Clibrd Extra' ;
  NTAServices.MainMenu.Items.Add(FMainMenuItem);

  // Sub Menus
  FCopyMenuItem := TMenuItem.Create(nil);
  FCopyMenuItem.Caption := 'Copy to clipboard';
  FCopyMenuItem.OnClick := Self.CopyToClipboard;
  FMainMenuItem.Add(FCopyMenuItem);

  FPasteMenuItem := TMenuItem.Create(nil);
  FPasteMenuItem.Caption := 'Paste from clipboard';
  FPasteMenuItem.OnClick := Self.PasteFromClipboard;
  FMainMenuItem.Add(FPasteMenuItem);
end;

destructor TClipboardWizard.Destroy;
begin
  if Assigned(FPasteMenuItem) then
    FreeAndNil(FPasteMenuItem);

  if Assigned(FCopyMenuItem) then
    FreeAndNil(FCopyMenuItem);

  if Assigned(FMainMenuItem) then
    FreeAndNil(FMainMenuItem);

  inherited;
end;


{ IOTANotifier }
procedure TClipboardWizard.AfterSave;
begin
end;

procedure TClipboardWizard.BeforeSave;
begin
end;

procedure TClipboardWizard.Destroyed;
begin
end;

procedure TClipboardWizard.Modified;
begin
end;

{ IOTAWizard }

function TClipboardWizard.GetIDString: string;
begin
  Result := 'Clipboard.Wizard7';
end;

function TClipboardWizard.GetName: string;
begin
  Result := 'Clipboard Wizard7';
end;

function TClipboardWizard.GetState: TWizardState;
begin
  Result := [];
end;

procedure TClipboardWizard.Execute;
begin
end;


end.
